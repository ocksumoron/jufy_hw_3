//
//  Chat.swift
//  hw3
//
//  Created by Даня on 30.11.14.
//  Copyright (c) 2014 mipt. All rights reserved.
//

import UIKit

var chat: Chat = Chat()

class Chat: NSObject {
    
    var chatMessages: [String]
    var senders: [String]
    
    override init() {
        chatMessages = [String]()
        senders = [String]()
    }
}
