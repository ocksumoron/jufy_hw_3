//
//  ViewController.swift
//  hw3
//
//  Created by Даня on 25.11.14.
//  Copyright (c) 2014 mipt. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var loginField: UITextField!
    
    @IBAction func loginButton(sender: UIButton) {
        let chatView = self.storyboard?.instantiateViewControllerWithIdentifier("chatPage") as ChatViewController
        
        chatUser.setName(loginField.text)
        
        self.navigationController?.pushViewController(chatView, animated: true)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

