//
//  ChatViewController.swift
//  hw3
//
//  Created by Даня on 28.11.14.
//  Copyright (c) 2014 mipt. All rights reserved.
//

import UIKit

class ChatViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var allMessages: UITableView!
    @IBOutlet weak var messageText: UITextField!
    @IBAction func sendMessage(sender: UIButton) {
        println("trying to")
        var message:PFObject = PFObject(className: "Messages")
        message["text"] = messageText.text
        message["sender"] = chatUser.name
        
        message.saveInBackgroundWithBlock {
            (success: Bool!, error: NSError!) -> Void in
            if success != nil {
                println("Object created with id: \(message.objectId)")
            } else {
                println("%@", error)
            }
        }
        println("should be done")
        messageText.text = ""
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        var timer = NSTimer.scheduledTimerWithTimeInterval(2.0, target: self, selector: Selector("updateChat"), userInfo: nil, repeats: true)

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func updateChat() {
        allMessages.reloadData()
        var query = PFQuery(className:"Messages")
        query.skip = chat.chatMessages.count
        query.findObjectsInBackgroundWithBlock {
            (objects: [AnyObject]!, error: NSError!) -> Void in
            if error == nil {
                // The find succeeded.
                NSLog("Successfully retrieved \(objects.count) messages.")
                // Do something with the found objects
                for rawObject in objects {
                    let object = rawObject as PFObject
                    chat.chatMessages.append(object["text"] as String)
                    chat.senders.append(object["sender"] as String)
                   // NSLog("%@", temp["sender"])
                }
            } else {
                // Log details of the failure
                NSLog("Error: %@ %@", error, error.userInfo!)
            }
        }
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        let center = NSNotificationCenter.defaultCenter()
        center.addObserver(self, selector: "keyboardOnScreen:", name: UIKeyboardDidShowNotification, object: nil)
        center.addObserver(self, selector: "keyboardOffScreen:", name: UIKeyboardDidHideNotification, object: nil)
        
    }
    
    func keyboardOnScreen(notification: NSNotification){
        let info: NSDictionary  = notification.userInfo!
        let kbSize = info.valueForKey(UIKeyboardFrameEndUserInfoKey)?.CGRectValue().size
        let contentInsets:UIEdgeInsets  = UIEdgeInsetsMake(0.0, 0.0, kbSize!.height, 0.0)
        scrollView.contentInset = contentInsets
        scrollView.scrollIndicatorInsets = contentInsets
        var aRect: CGRect = self.view.frame
        aRect.size.height -= kbSize!.height
        //you may not need to scroll, see if the active field is already visible
        if (!CGRectContainsPoint(aRect, messageText.frame.origin) ) {
            let scrollPoint:CGPoint = CGPointMake(0.0, messageText.frame.origin.y - kbSize!.height)
            scrollView.setContentOffset(scrollPoint, animated: true)
        }
        
    }
    
    
    func keyboardOffScreen(notification: NSNotification){
        let contentInsets:UIEdgeInsets = UIEdgeInsetsZero
        scrollView.contentInset = contentInsets
        scrollView.scrollIndicatorInsets = contentInsets
    }

    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        println("\(chat.chatMessages.count)")
        return chat.chatMessages.count
    }
    
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell: UITableViewCell = UITableViewCell(style: UITableViewCellStyle.Subtitle, reuseIdentifier: "default")
        cell.textLabel.text = chat.chatMessages[chat.chatMessages.count - 1 - indexPath.row] as String
        cell.detailTextLabel!.text = chat.senders[chat.chatMessages.count - 1 - indexPath.row] as String
        return cell
    }

}
